# CURA6 dataset

This is CURA6 dataset. 

In folder 'learning_data' there are 91 slices of random movements of the CURA6 robot. Each learining slice has about 10,000 samples. While folder 'test_dataset' contains another 91 slices of data with shorter slices. The 'violence_data' has 78 slices with collisions marked in files: '..._punch_timestamp.npy'.

From the name you can get the velocity of robot (from 10 to 70 % of maximum velocity) and its load [gram] which takes value from the list: 
```python 
[0, 401, 852, 1086, 1401, 1950, 2116, 2368, 2832, 3222, 3683, 4122, 4652]
``` 
Data are saved as numpy standard. To unpack them just use this code:
```python
import numpy as np
file_name = 'learning_data/data_10_0g.npz'
data = np.load(file_name)
time_stamp = data.get('robot_time_stamp')     # relative in seconds
sequence_stamp = data.get('robot_seq')        # number of sequence
robot_movement = data.get('robot_movement')   # robot movements [True/False]
robot_position = data.get('robot_position')   # robot positions [rad]
robot_velocity = data.get('robot_velocities') # robot velocities [rad/sec]
robot_mcurrent = data.get('robot_iq')         # robots motors current [A]
target_velocity = data.get('target_velocity') # target velocities [rad/sec]
```

If You want to use the dataset pleas cite us:
```bibitex
@article{cura6_dataset,
    autor = {Czubenko, M. and Kowalczuk, Z.},
    title = {Simple neural network for collision detection of collaborative robots},
    year = {2021},
    journal = {Sensors},
    number = {?},
    volumen = {?},
    organization = {Intema Sp. z o.o.}
}
```
